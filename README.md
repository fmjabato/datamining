# README (ES)#

Todo el código de este repositorio es opensource bajo licencia CopyLeft de libre divulgación y modificación, uso no comercial y respeto de autoría.

Este repositorio contiene código para la asignatura de Minería de Datos de los estudios de Ingeniería de la Salud, mención en bioinformática, de la Universidad de Málaga.

Existe un PDF que contiene la información de todos los proyectos, exceptuando el final. El código se encuentra en la carpeta SRC y los sets de datos utilizado se encuentran en la carpeta DATASETS.

Todo el código ha sido implementado por Fernando Moreno Jabato. Inclusiones o referencias de otros autores son especificadas en el propio código.

# README (EN)#

All code of this repository are opensource under CopyLeft of free share, modificate, non comercial, respect authorship.

This repository contains code for "Minería de Datos" (Data mining) subject of "Ingeniería de la Salud mención en bioinfromática" (BioIT) studies of University of Malaga.

There are a PDF with all projects, less final project, specified. All codes are in SRC folder and data sets used are in DATASETS folder.

All the code has been done by Fernando Moreno Jabato. Inclusions or references to other authors are specified on the code.